﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class object_movement : MonoBehaviour
{
    private Rigidbody2D rb;

    public float forwardForce = 2000f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        rb.AddForce(new Vector2(0, -forwardForce*Time.deltaTime));
    }
}
