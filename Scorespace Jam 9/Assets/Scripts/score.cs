﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score : MonoBehaviour
{
    public GameManager GameManager;

    public Text scoretext;
    private float Maximum = 0f;
    private bool onetime = false;


    private void Update()
    {
        if (GameManager.gameHasEnded == false)
        {
            scoretext.text = (GameManager.Center.y).ToString("#.00");
        }
        else
        {
            if (!onetime)
            {
                CheckMax();
                onetime = true;
            }
        }
    }

    public void CheckMax()
    {
        Maximum = Mathf.Round((GameManager.Center.y) * 100f) / 100f;
        if (Maximum > PlayerPrefs.GetFloat("MaxScore"))
        {
            PlayerPrefs.SetFloat("MaxScore", Maximum);
        }
    }

}
