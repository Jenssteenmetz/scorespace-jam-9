﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Collision : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D collisioninfo)
    {
        if (collisioninfo.tag == "obstacle")
        {
            GameManager.instance.EndGame();
        }
    }

}