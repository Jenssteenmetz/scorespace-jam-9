﻿using UnityEngine;

public class delete_blocks : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("obstacle"))
        {
            Destroy(collision.gameObject);
        }
    }
}
