﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Transform[] spawnPoints;

    public bool gameHasEnded = false;
    private float restartDelay = 3f;

    public enum ObstacleLocation {Left, Mid, Right}

    [Header("Obstacle Spawner Vars")]
    public float randomSpawnRadius;

    [Header("Resources")]
    [SerializeField] private GameObject obstacleWarningTextBalloonPf;
    [SerializeField] private Sprite obstacleWarningIconLeft;
    [SerializeField] private Sprite obstacleWarningIconMid;
    [SerializeField] private Sprite obstacleWarningIconRight;

    public GameObject rockPf;

    private void Awake()
    {
        instance = this;
    }

    private Vector2 LeftHand = new Vector2(-1f, 0f);
    private Vector2 RightHand = new Vector2(1f, 0f);

    public float BodyOffset = 1f;
    public Vector2 Center = new Vector2(0f, 0f);

    public float sidewaysOffset = 1f;
    public float upwardsOffset = 1f;
    private Vector2 LeftBorder = new Vector2(-1f, 1f);
    private Vector2 RightBorder = new Vector2(1f, 1f);

    private float dist = 0f;
    public float CircleSize = 3f;

    private Vector2 clicked;
    private Vector2 heading;
    private Vector2 distance;


    //for testing
    public GameObject hands;
    public GameObject body;
    GameObject GOL = null;
    GameObject GOR = null;
    GameObject BODYBUILD = null;

    private void Start()
    {
        Center = new Vector2(RightHand.x + (LeftHand.x - RightHand.x) / 2, RightHand.y + (LeftHand.y - RightHand.y) / 2 - BodyOffset);
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            clicked = new Vector2(Input.mousePosition.x - 270f, Input.mousePosition.y - 225f);

            dist = Vector2.Distance(clicked, LeftBorder);

            //testing
            if (GOL != null)
            {
                Destroy(GOL);
            }
            if (BODYBUILD != null)
            {
                Destroy(BODYBUILD);
            }


            if (dist < CircleSize)
            {
                LeftHand = clicked;
            }
            else
            {
                heading = clicked - LeftBorder;
                distance = heading.normalized;
                LeftHand = LeftBorder + distance;
            }

            Center = new Vector2(RightHand.x + (LeftHand.x - RightHand.x) / 2, RightHand.y + (LeftHand.y - RightHand.y) / 2 - BodyOffset);
            LeftBorder = Center + new Vector2(-sidewaysOffset, upwardsOffset);
            RightBorder = Center + new Vector2(sidewaysOffset, upwardsOffset);

            //testing
            GOL = Instantiate(hands, LeftHand, Quaternion.identity);
            BODYBUILD = Instantiate(body, Center, Quaternion.identity);
        }

        if (Input.GetMouseButtonDown(1))
        {
            clicked = new Vector2(Input.mousePosition.x - 270f, Input.mousePosition.y - 225f);

            dist = Vector2.Distance(clicked, RightBorder);

            //testing
            if (GOR != null)
            {
                Destroy(GOR);
            }
            if (BODYBUILD != null)
            {
                Destroy(BODYBUILD);
            }


            if (dist < CircleSize)
            {
                RightHand = clicked;
            }
            else
            {
                heading = clicked - RightBorder;
                distance = heading.normalized;
                RightHand = RightBorder + distance;
            }

            //testing
            GOR = Instantiate(hands, RightHand, Quaternion.identity);
            BODYBUILD = Instantiate(body, Center, Quaternion.identity);

            Center = new Vector2(RightHand.x + (LeftHand.x - RightHand.x) / 2, RightHand.y + (LeftHand.y - RightHand.y) / 2 - BodyOffset);
            LeftBorder = Center + new Vector2(-sidewaysOffset, upwardsOffset);
            RightBorder = Center + new Vector2(sidewaysOffset, upwardsOffset);
        }
    }

    public void Warning(Collider other)
    {
        float X = other.transform.position.x;
    }

    public void EndGame()
    {

        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            //FindObjectOfType<SoundManager>().Play("falling off"); Little prep for Sounds
            Invoke("Restart", restartDelay);
        }

    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void SpawnObstacleWarningTextBalloon(Vector3 spawnPosition, ObstacleLocation obstacleLocation)
    {
        Transform obstacleWarningGO = Instantiate(obstacleWarningTextBalloonPf, spawnPosition, Quaternion.identity).transform;
        SpriteRenderer sr = obstacleWarningGO.Find("LeftMidRightIcon").GetComponent<SpriteRenderer>();
        switch (obstacleLocation)
        {
            case ObstacleLocation.Left:
                sr.sprite = obstacleWarningIconLeft;
                break;
            case ObstacleLocation.Mid:
                sr.sprite = obstacleWarningIconMid;
                break;
            case ObstacleLocation.Right:
                sr.sprite = obstacleWarningIconRight;
                break;
            default:
                sr.sprite = obstacleWarningIconMid;
                break;
        }
    }

    private void StartNewBigWave(int _waveAmount, float _timeBetweenWaves)
    {
        RockSpawner.CreateRockSpawner(_waveAmount, _timeBetweenWaves);
    }

    private void OnDrawGizmos()
    {
        foreach (Transform spawnPoint in spawnPoints)
        {
            Gizmos.DrawWireSphere(spawnPoint.position, randomSpawnRadius);
        }
    }
}
