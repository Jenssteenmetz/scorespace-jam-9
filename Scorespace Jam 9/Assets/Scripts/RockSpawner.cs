﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockSpawner : MonoBehaviour
{
    public float spawnTimer;
    public float timeBetweenWaves;

    private int spawnAmount = 2;
    private int waveCounter;

    public static RockSpawner CreateRockSpawner(int _waveAmount, float _timeBetweenWaves)
    {
        RockSpawner rs = new GameObject("ObstacleSpawner", typeof(RockSpawner)).GetComponent<RockSpawner>();
        rs.Setup(_waveAmount, _timeBetweenWaves);
        return rs;
    }

    public void Setup(int _waveAmount, float _timeBetweenWaves)
    {
        waveCounter = _waveAmount;
        timeBetweenWaves = _timeBetweenWaves;

        spawnTimer = _timeBetweenWaves;
    }

    // Update is called once per frame
    void Update()
    {
        if(spawnTimer > 0)
        {
            spawnTimer -= Time.deltaTime;
        }
        else
        {
            SpawnBlocks();
            waveCounter--;
            //if wavecounter == 0 --> destroy this rockspawner
        }
    }

    private void SpawnBlocks()
    {
        for (int j = 0; j < spawnAmount; j++)
        {
            Vector3 randomSpawnVector = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f)).normalized * GameManager.instance.randomSpawnRadius;
            Instantiate(GameManager.instance.rockPf, GameManager.instance.spawnPoints[j].position + randomSpawnVector, Quaternion.identity);
        }
    }
}
