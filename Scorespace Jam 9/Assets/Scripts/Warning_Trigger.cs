﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warning_Trigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        GameManager.instance.SpawnObstacleWarningTextBalloon(new Vector3(60, 0), GameManager.ObstacleLocation.Right);
    }
}
